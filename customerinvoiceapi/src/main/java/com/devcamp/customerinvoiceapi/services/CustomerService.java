package com.devcamp.customerinvoiceapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.customerinvoiceapi.models.Customer;

@Service
public class CustomerService {
    Customer customer1 = new Customer(1,"Huong",10);
    Customer customer2 = new Customer(2,"Katy",20);
    Customer customer3 = new Customer(3,"Nicky",10);
    Customer customer4 = new Customer(4,"Tom",30);
    Customer customer5 = new Customer(5,"Jerry",5);

    public ArrayList<Customer> getAllCustomers(){
        ArrayList<Customer> allCustomerList = new ArrayList<>();
        allCustomerList.add(customer1);
        allCustomerList.add(customer2);
        allCustomerList.add(customer3);
        allCustomerList.add(customer4);
        allCustomerList.add(customer5);
        return allCustomerList;
    }

}
